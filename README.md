# Onboarding
This is a general guide detailing the steps to take when onboarding on your first day at Bankable. Some of the following steps may not apply, just skip onto the next section. This guide is made with the assumption you are on a machine running Windows 10.

## Table of Contents
- [Login to Gmail and setup your account](#login-to-gmail-and-setup-your-account)
- [Newcomer Documentation Checklist](#newcomer-documentation-checklist)
- [Access for Canada Square Office](#access-for-canada-square-office)
- [Getting permission for Access](#getting-permission-for-access)
- [GitLab Repository](#gitlab-repository)
    - [Cloning a Repository for the first time](#cloning-a-repository-for-the-first-time)
    - [Maven Settings](#maven-settings)
- [Download and Install Applications](#download-and-install-applications)
    - [Installing WSL 2 and Ubuntu](#installing-wsl-2-and-ubuntu)
    - [Using Java 8/11 with JEnv and Maven](#using-java-811-with-jenv-and-maven)
    - [OpenVPN setup](#openvpn-setup)
    - [Postman setup](#postman-setup)
- [Connecting to Dev, QA and other environments](#connecting-to-dev-qa-and-other-environments)

## Login to Gmail and setup your account
1. You should have received some credentials via your personal email containing your work email and password. Use this to login to Gmail account
2. Follow the steps to activate your account
3. Once you are logged in, modify the settings to meet your preferences e.g adding a signature

## Newcomer Documentation Checklist
When joining Bankable, there are some documents you will be required to fill out. You should receive an email for each of these:

- [ ] Sign PCI DSS Compliance Document
- [ ] Sign [GPRD Photo Consent](https://bankablel42.sharepoint.com/:w:/r/sites/HR/_layouts/15/Doc.aspx?sourcedoc=%7B9ED6DEE7-3750-45A5-97B3-579E3F26EC60%7D&file=GDPR%20Photo%20Consent%20.docx&action=default&mobileredirect=true) form and hand in to HR
- [ ] Accept invitation to join Bob, the HR platform
- [ ] Fill in form from HireRight for Disclosure and Barring Service check
- [ ] Complete [HRMC form](https://public-online.hmrc.gov.uk/lc/content/xfaforms/profiles/forms.html?contentRoot=repository:///Applications/NIC_A/1.0/SC_202122&template=SC2.xdp) and hand in to HR

## Access for Canada Square Office
To get physical access to One Canada Square, there is an onboarding process that is to be completed with Level39, the space provider for the office. Inductions are typically done on a Tuesday or Thursday and you are required to fill in this [new starter form](https://about.level39.co/new-starter/) to sign up.

## Getting permission for Access
You will need to contact an administrator such as [Davide Porracchio](mailto:dp@bnkbl.com) for access to the following:

- GitLab
- JFrog

## GitLab Repository
1. Login to [GitLab](https://gitlab.com/bankable-organisation) using Google Authentication
2. Visit the [Bankable Organisation](https://gitlab.com/bankable-organisation) and ensure repositories are viewable

### Cloning a Repository for the first time
1. Open Windows PowerShell
2. Generate an SSH using the command `ssh-keygen -t rsa -b 4096 -C <key name or email>` and follow steps until a key pair is generated
3. Enable SSH agent to auto start using `Set-Service -Name ssh-agent -StartupType Automatic`
4. Manually start for the first time using `Start-Service ssh-agent`
5. Add your SSH key to your agent by running `ssh-add C:\Users\<user>\.ssh\id_rsa`
6. Go to [GitLab SSH Key](https://gitlab.com/-/profile/keys) page and add your ssh key (id_rsa.pub)
6. Pick a repository to git clone and test it works e.g. `git clone git@gitlab.com:bankable-organisation/bankable-java/accounting-service.git`

### Maven Settings
To be able to pull dependencies you will need [settings.xml](https://bankablel42-my.sharepoint.com/:u:/g/personal/enayet_hussain_bnkbl_com/EbetZiFBJ8hLhmiVWkn5AlQBHsFBjuQHoag_XWUPTfEZFg?e=QIHL8X) in your .m2 folder 
`C:\Users\<user>\.m2`

## Download and Install Applications
> Note: If you do not have the correct privileges to install the following applications please send a message to `servicedesk@bnkbl.com` or reach out to Jayme Bruce on Microsoft Teams

- [Microsoft Teams](https://www.microsoft.com/en-gb/microsoft-teams/download-app#desktopAppDownloadregion)
- [IntelliJ](https://www.jetbrains.com/idea/download/download-thanks.html?platform=windows&code=IIC)
- [DBeaver](https://dbeaver.io/files/dbeaver-ce-latest-x86_64-setup.exe)
- [Java 8](https://www.oracle.com/uk/java/technologies/javase/javase8-archive-downloads.html#license-lightbox) & [Java 11](https://www.oracle.com/uk/java/technologies/javase/jdk11-archive-downloads.html#license-lightbox) (Optionally with JEnv)
- WSL 2 with [Ubuntu](https://apps.microsoft.com/store/detail/ubuntu/9PDXGNCFSCZV?hl=en-us&gl=US) (Optional)
- [Git Bash](https://git-scm.com/download/win)
- [OpenVPN](https://openvpn.net/community-downloads/)
- [Maven](https://dlcdn.apache.org/maven/maven-3/3.8.5/binaries/apache-maven-3.8.5-bin.zip)
- [Postman](https://dl.pstmn.io/download/latest/win64)

### Installing WSL 2 and Ubuntu
> Note: WSL 2 requires virtualization to be enabled from the BIOS
1. Launch Command Prompt as an Administrator
2. Run command `wsl --install`
3. Once install has completed, restart your machine
4. Install [Ubuntu](https://apps.microsoft.com/store/detail/ubuntu/9PDXGNCFSCZV?hl=en-us&gl=US) from the Microsoft Store

### Using Java 8/11 with JEnv and Maven
1. Clone [JEnv](https://github.com/FelixSelter/JEnv-for-Windows) to a local folder
2. Download [Maven](https://dlcdn.apache.org/maven/maven-3/3.8.5/binaries/apache-maven-3.8.5-bin.zip) and extract to a local folder
3. Copy path of JEnv location e.g. `C:\Users\<user>\Downloads\Tools\JEnv-for-Windows`
4. Press Start and open *Edit environment variables for your account*
5. Select `Path` variable and press *Edit*
6. Add a new entry with your copied JEnv folder location
7. Add another entry with the location of your Maven bin folder e.g. `C:\Users\<user>\Downloads\Tools\apache-maven-3.8.5\bin`
8. Download then install both [Java 11](https://www.oracle.com/uk/java/technologies/javase/jdk11-archive-downloads.html#license-lightbox) and [Java 8](https://www.oracle.com/uk/java/technologies/javase/javase8-archive-downloads.html#license-lightbox)
9. Open Command Prompt and run similar commands to the following to add your JDKs to JEnv:
```
jenv add jdk11 "C:\Program Files\Java\jdk-11.0.15"
jenv add jdk8 "C:\Program Files\Java\jdk1.8.0_202"
```
10. Once added successfully, we can set our preferred global Java version using `jenv change jdk11` and local version using `jenv use jdk11`

### OpenVPN setup
1. Install OpenVPN client
2. Right click OpenVPN in system tray > Import > Import file...
3. Select your [dev.ovpn](https://bankablel42-my.sharepoint.com/:u:/g/personal/enayet_hussain_bnkbl_com/Ec9k7HBk9Z1Kq8Wmps-yuPYBcLFWkygAF9Ai5vzqI19ejA?e=J6Q6p0) file given to you by the team

### Postman setup
1. Install Postman client
2. Select Collections > press + button
3. Add the relevant collections:

Colections Available:
- [CA Swagger UAT V2](https://gitlab.com/enayet.hussain/onboarding/-/raw/main/CA_Swagger_UAT_V2.postman_collection.json?inline=false)
- [Discover UAT](https://gitlab.com/enayet.hussain/onboarding/-/raw/main/DISCOVER_UAT.postman_collection.json?inline=false)
- [Paysafe UAT](https://gitlab.com/enayet.hussain/onboarding/-/raw/main/Paysafe_UAT.postman_collection.json?inline=false)
- [Swagger UAT V2](https://gitlab.com/enayet.hussain/onboarding/-/raw/main/Swagger_UAT_V2.postman_collection.json?inline=false)

## Connecting to Dev, QA and other environments
1. Add the following entries into your hosts file with admin privileges `C:\Windows\System32\drivers\etc\hosts`
<details>
<summary>Hosts file entries</summary>

```
127.0.0.1 db.dev.com
127.0.0.1 processor.dev.com
127.0.0.1 service.dev.com
127.0.0.1 website.dev.com

127.0.0.1 db.qa.com
127.0.0.1 processor.qa.com
127.0.0.1 service.qa.com
127.0.0.1 website.qa.com

127.0.0.1 db.monster.com
127.0.0.1 processor.monster.com
127.0.0.1 service.monster.com
127.0.0.1 website.monster.com

127.0.0.1 db.int.com
127.0.0.1 processor.int.com
127.0.0.1 service.int.com
127.0.0.1 website.int.com

127.0.0.1 db.fx.com
127.0.0.1 processor.fx.com
127.0.0.1 service.fx.com
127.0.0.1 website.fx.com
```

</details>

2. Clone the [acceptance-for-life](https://gitlab.com/bankable-organisation/bankable-java/acceptance-for-life) project to your local machine
`git clone git@gitlab.com:bankable-organisation/bankable-java/acceptance-for-life.git`

3. Navigate to the `scripts` folder in the `acceptance-for-life` project

4. Give permission to `ports_tunnel_win.sh` and `test_key.pem` to run:

```
chmod +x ports_tunnel_win.sh
chmod +x test_key.pem
```

5. Run the script with `on` as the only parameter `./ports_tunnel_win.sh on` and type your desired environment

### Connecting to custom environments

When using a custom environment you must modify your OpenVPN config in order to connect

1. Right click on OpenVPN in the windows task bar
2. Select Edit Config
3. Add the following line under any existing route:

```
route <environment_name>.bnkblsolutions.com
```
